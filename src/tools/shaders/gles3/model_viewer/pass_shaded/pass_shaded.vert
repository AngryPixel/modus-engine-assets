#version 300 es

#include "engine/globals.glsl"

layout(location = MODUS_VERTEX_SLOT) in vec3 vertex_position;
layout(location = MODUS_NORMAL_SLOT) in vec3 normal;
layout(location = MODUS_TEXTURE_SLOT) in vec2 tex_coord;
layout(location = MODUS_TANGENT_SLOT) in vec3 a_tangent;
layout(location = MODUS_BINORMAL_SLOT) in vec3 a_binormal;

uniform mat4 model;
uniform mat4 normal_matrix;

out vec3 var_frag_pos;
out vec3 var_normal;
out vec2 var_tex_coord;
out mat3 var_TBN;
void main() {
    var_normal = vec3(normal_matrix * vec4(normal,1.0));
    var_frag_pos = vec3(model * vec4(vertex_position, 1.0));
    var_tex_coord = tex_coord;

/*
    vec3 N = normalize(vec3(model * vec4(normal, 0)));
    vec3 T = normalize(vec3(model * vec4(a_tangent, 0)));
    vec3 B = normalize(vec3(model * vec4(a_binormal, 0)));
    var_TBN = transpose(mat3(T,B, N));*/

    vec3 N = normalize(vec3(model * vec4(normal, 0)));
    vec3 T = normalize(vec3(model * vec4(a_tangent, 0)));
    T = normalize(T - dot(T,N)* N);
    vec3 B = cross(N,T);
    var_TBN = transpose(mat3(T,B, N));


    gl_Position = proj_view * model * vec4 (vertex_position, 1.0);
}

