#include "engine/materials.glsl"

layout(location = 0) out vec4 frag_colour;

in vec3 var_normal;
in vec3 var_frag_pos;
in vec2 var_tex_coord;
in mat3 var_TBN;

uniform sampler2D map_diffuse;
uniform sampler2D map_normal;
uniform int enable_diffuse;
uniform int enable_normal;
uniform int enable_light;

void main() {

    float diff = 1.0;
    if (enable_light != 0) {
        vec3 ligth_pos = vec3(1.0, 1.0, 1.0);
        // For directional light, the direction is from the point
        // to the origin.
        vec3 ligth_dir = ligth_pos;
        //highp vec3 ligth_dir = normalize(ligth_pos - var_frag_pos);


        vec3 norm;
        if (modus_material_has_normal_map(modus_materials[modus_material_index].flags) && enable_normal != 0) {
            norm = texture(map_normal, var_tex_coord).rgb;
            norm = normalize(norm * 2.0 - 1.0);
            // apply tbn to normal
            //norm = normalize(var_TBN * norm);
            // Apply to transpose TBN
            ligth_dir = var_TBN *ligth_dir;
        } else {
            norm = normalize(var_normal);
        }

        diff = max(dot(norm, ligth_dir), 0.0);
    }

    vec4 diffuse;
    if (modus_material_has_diffuse_map(modus_materials[modus_material_index].flags) && enable_diffuse != 0) {
        diffuse = texture(map_diffuse, var_tex_coord);
    } else {
        diffuse = modus_materials[modus_material_index].color_diffuse;
    }

    frag_colour = diff * diffuse;
    // Apply Gamma Correction
    //float gamma = 2.2;
    //frag_colour.rgb = pow(frag_colour.rgb, vec3(1.0/gamma));
}
