#
# Utilities to create collision meshes
#
if (NOT TARGET modus_mk_collision_mesh)
    message(FATAL_ERROR "Could not find modus_mk_collision_mesh target")
endif()

function(modus_create_collision_mesh_from_model)

    cmake_parse_arguments(arg "" "INPUT;OUTPUT" "EXTRA_ARGS" ${ARGN})

    add_custom_command(OUTPUT ${arg_OUTPUT}
        DEPENDS ${arg_INPUT}
        COMMAND modus_mk_collision_mesh
            -o ${arg_OUTPUT}.tmp
            -i ${arg_INPUT}
            ${arg_EXTRA_ARGS}
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${arg_OUTPUT}.tmp
            ${arg_OUTPUT}
        COMMENT "Generating collision mesh ${arg_OUTPUT}"
    )
endfunction()

