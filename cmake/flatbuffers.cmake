#
# Flatbuffers related utilitlies
#

if (NOT MODUS_ENGINE_FLATBUFFER_SCHEMA_ROOT_DIR)
    message(FATAL_ERROR "MODUS_ENGINE_FLATBUFFER_SCHEMA_ROOT_DIR is not defined.")
endif()

function(modus_convert_flatbuffer schema_path input return_output_name)

    file(RELATIVE_PATH relative_path ${MODUS_ASSET_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
    set(output_dir "${MODUS_ASSETS_BIN_DIR}/${relative_path}")

    get_filename_component(bin_name ${input} NAME_WLE)

    set(output_name "${output_dir}/${bin_name}")
    set(input_file "${CMAKE_CURRENT_SOURCE_DIR}/${input}")

    add_custom_command(OUTPUT "${output_name}"
        DEPENDS ${input_file}
        COMMAND ${modus_flatc_bin}
            -I "${MODUS_ENGINE_FLATBUFFER_SCHEMA_ROOT_DIR}"
            -I "${MODUS_ENGINE_SOURCE_DIR}/../engine_modules/graphics/flatbuffers"
            -b "${MODUS_ENGINE_SOURCE_DIR}/../engine_modules/graphics/flatbuffers/${schema_path}"
            ${input_file}
        COMMAND ${CMAKE_COMMAND} -E rename "${bin_name}.bin" "${output_name}"
        COMMENT "Generating binary asset ${output_name}"
    )

    set(${return_output_name} ${output_name} PARENT_SCOPE)
endfunction()


function(modus_convert_images)
    cmake_parse_arguments(arg "" "TARGET" "FILES" ${ARGN})
    set(converted_list "")
    foreach(input IN LISTS arg_FILES)
        modus_convert_flatbuffer("engine/graphics/image.fbs" ${input} result)
        list(APPEND converted_list ${result})
    endforeach()
    add_custom_target(${arg_TARGET}
        DEPENDS ${converted_list}
    )
    add_dependencies(make_bin_assets ${arg_TARGET})
endfunction()

function(modus_convert_fonts)
    cmake_parse_arguments(arg "" "TARGET" "FILES" ${ARGN})
    set(converted_list "")
    foreach(input IN LISTS arg_FILES)
        modus_convert_flatbuffer("engine/graphics/font.fbs" ${input} result)
        list(APPEND converted_list ${result})
    endforeach()
    add_custom_target(${arg_TARGET}
        DEPENDS ${converted_list}
    )
    add_dependencies(make_bin_assets ${arg_TARGET})
endfunction()

function(modus_convert_meshes target)
    cmake_parse_arguments(arg ""  "" "FILES" ${ARGN})
    set(converted_list "")
    foreach(input IN LISTS arg_FILES)
        modus_convert_flatbuffer("engine/graphics/mesh.fbs" ${input} result)
        list(APPEND converted_list ${result})
    endforeach()
    add_custom_target(${target}
        DEPENDS ${converted_list}
    )
    add_dependencies(make_bin_assets ${target})
endfunction()

function(modus_convert_model target)
    cmake_parse_arguments(arg ""  "" "FILES" ${ARGN})
    set(converted_list "")
    foreach(input IN LISTS arg_FILES)
        modus_convert_flatbuffer("engine/graphics/model.fbs" ${input} result)
        list(APPEND converted_list ${result})
    endforeach()
    add_custom_target(${target}
        DEPENDS ${converted_list}
    )
    add_dependencies(make_bin_assets ${target})
endfunction()

function(modus_bin_copy target)
    cmake_parse_arguments(arg "" "" "FILES" ${ARGN})

    foreach(input IN LISTS arg_FILES)
        file(RELATIVE_PATH relative_path ${MODUS_ASSET_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
        set(output_dir "${MODUS_ASSETS_BIN_DIR}/${relative_path}")

        get_filename_component(bin_name ${input} NAME)

        set(output_name "${output_dir}/${bin_name}")
        set(input_file "${CMAKE_CURRENT_SOURCE_DIR}/${input}")

        add_custom_command(OUTPUT "${output_name}"
            DEPENDS ${input_file}
            COMMAND ${CMAKE_COMMAND} -E copy
                ${input_file}
                ${output_name}
            COMMENT "Copying binary assert ${output_name}"
        )
        list(APPEND converted_list ${output_name})
    endforeach()
    add_custom_target(${target}
        DEPENDS ${converted_list}
    )
    add_dependencies(make_bin_assets ${target})
endfunction()
