#
# Utilities to create meshes
#

if (NOT TARGET modus_mk_mesh)
    message(FATAL_ERROR "Could not find modus_mk_mesh target")
endif()

if (NOT TARGET modus_lod_merger)
    message(FATAL_ERROR "Could not find modus_lod_merger target")
endif()


function(modus_create_mesh_from_model)

    cmake_parse_arguments(arg "SKIP_PACKING" "INPUT;OUTPUT;MATERIAL_DIR" "EXTRA_ARGS;BYPRODUCTS" ${ARGN})

    set(pack_args)
    if (NOT arg_SKIP_PACKING)
        set(pack_args --pack-all)
    endif()

    add_custom_command(OUTPUT ${arg_OUTPUT} ${arg_BYPRODUCTS}
        DEPENDS ${arg_INPUT} modus_mk_mesh
        COMMAND modus_mk_mesh
            -o ${arg_OUTPUT}
            -t assimp
            ${pack_args}
            --assimp-file ${arg_INPUT}
            -m ${arg_MATERIAL_DIR}
            ${arg_EXTRA_ARGS}
        COMMENT "Generating mesh ${arg_OUTPUT}"
    )
endfunction()

function(modus_create_mesh_from_modelv2)

    cmake_parse_arguments(arg "SKIP_PACKING" "OUTPUT;MATERIAL_DIR" "INPUTS;EXTRA_ARGS;BYPRODUCTS" ${ARGN})

    set(pack_args)
    if (NOT arg_SKIP_PACKING)
        set(pack_args --pack-all)
    endif()

    # Generate individual meshes
    set(generated_list)
    foreach(input IN LISTS arg_INPUTS)
        get_filename_component(input_name ${input} NAME)
        set(output_file "${CMAKE_CURRENT_BINARY_DIR}/${input_name}")
        add_custom_command(OUTPUT ${output_file}
            DEPENDS ${input} modus_mk_mesh
            COMMAND modus_mk_mesh
                -o ${output_file}.tmp
                -t assimp
                ${pack_args}
                --assimp-file ${input}
                -m ${CMAKE_CURRENT_BINARY_DIR}
                ${arg_EXTRA_ARGS}
            COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${output_file}.tmp
                ${output_file}
            COMMENT "Generating intermediate mesh ${output_file}"
        )
        list(APPEND generated_list ${output_file})
    endforeach()

    # Generate combined lod mesh
    add_custom_command(OUTPUT ${arg_OUTPUT}
        DEPENDS ${generated_list} modus_mk_mesh
        COMMAND modus_lod_merger
            -o ${arg_OUTPUT}.tmp
            ${generated_list}
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${arg_OUTPUT}.tmp
            ${arg_OUTPUT}
        COMMENT "Generating mesh ${arg_OUTPUT}"
    )

endfunction()
