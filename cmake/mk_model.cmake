#
# Utilities to create models
#


function(modus_create_model)
    cmake_parse_arguments(arg "" "INPUT;OUTPUT" "DEPENDS" ${ARGN})

    get_filename_component(bin_name ${arg_INPUT} NAME_WLE)
    add_custom_command(OUTPUT "${arg_OUTPUT}"
        DEPENDS ${arg_INPUT}
        COMMAND ${modus_flatc_bin}
            -I "${MODUS_ENGINE_FLATBUFFER_SCHEMA_ROOT_DIR}"
            -I "${MODUS_ENGINE_SOURCE_DIR}/../engine_modules/graphics/flatbuffers"
            -b "${MODUS_ENGINE_SOURCE_DIR}/../engine_modules/graphics/flatbuffers/engine/graphics/model.fbs"
            ${arg_INPUT}
        COMMAND ${CMAKE_COMMAND} -E rename "${bin_name}.bin" "${arg_OUTPUT}.tmp"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${arg_OUTPUT}.tmp
            ${arg_OUTPUT}
        COMMENT "Generating model ${arg_OUTPUT}"
    )
endfunction()
