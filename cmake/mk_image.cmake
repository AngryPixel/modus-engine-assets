#
# Utilities to create images
#

if (NOT TARGET modus_mk_image)
    message(FATAL_ERROR "Could not find modus_mk_image target")
endif()


find_program(etcpack-bin NAMES etcpack)
if (NOT etcpack-bin)
    message(WARNING "Could not locate etcpack binary, ETC2 compression disabled\n")
else()
    message(STATUS "Found etcpack: ${etcpack-bin}")
endif()

# Final output  = output_dir + type + output path
function(modus_create_image)

    cmake_parse_arguments(arg "SKIP_LOW_RES"
        "INPUT;OUTPUT_DIR;OUTPUT_PATH;TYPE;OUTPUT_IMAGE_LIST"
        "ASTC_ARGS;EXTRA_ARGS;ETC2_ARGS" ${ARGN}
    )

    set(image_list)

    set(input_depends)
    if (arg_TYPE STREQUAL "cube_map")
        get_filename_component(cubemap_dir ${arg_INPUT} DIRECTORY)
        file(GLOB input_depends "${cubemap_dir}/*")
    else()
        set(input_depends ${arg_INPUT})
    endif()

    # astc disabled as it's only properly supported on Intel GPU
    #if (astcenc-bin)
    #    set(astc_output "${arg_OUTPUT_DIR}/astc/${arg_OUTPUT_PATH}")
    #    list(APPEND image_list ${astc_output})
    #    # ASTC generation
    #    add_custom_command(OUTPUT ${astc_output}
    #        DEPENDS ${input_depends}
    #        COMMAND modus_mk_image
    #            -t ${arg_TYPE}
    #            -o ${astc_output}
    #            -i ${arg_INPUT}
    #            -c astc
    #            --astc-bin ${astcenc-bin}
    #            ${arg_ASTC_ARGS}
    #            ${arg_EXTRA_ARGS}
    #            COMMENT "Generating ASTC compressed image ${astc_output}"
    #    )
    #endif()

    if (etcpack-bin)
        set(etc2_output "${arg_OUTPUT_DIR}/etc2/${arg_OUTPUT_PATH}")
        list(APPEND image_list ${etc2_output})
        # ETC2 generation
        add_custom_command(OUTPUT ${etc2_output}
            DEPENDS ${input_depends}
            COMMAND modus_mk_image
                -t ${arg_TYPE}
                -o ${etc2_output}.tmp
                -i ${arg_INPUT}
                -c etc2
                --etcpack-bin ${etcpack-bin}
                --etcpack-fast
                ${arg_ETC2_ARGS}
                ${arg_EXTRA_ARGS}
            COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${etc2_output}.tmp
                ${etc2_output}
            COMMENT "Generating ETC2 compressed image ${etc2_output}"
        )
    endif()
    if (etcpack-bin AND NOT arg_SKIP_LOW_RES)
        set(etc2_output "${arg_OUTPUT_DIR}/etc2_low/${arg_OUTPUT_PATH}")
        list(APPEND image_list ${etc2_output})
        # ETC2 generation
        add_custom_command(OUTPUT ${etc2_output}
            DEPENDS ${input_depends}
            COMMAND modus_mk_image
                -t ${arg_TYPE}
                -o ${etc2_output}.tmp
                -i ${arg_INPUT}
                -c etc2
                --etcpack-bin ${etcpack-bin}
                --etcpack-fast
                --max-width 512
                --max-height 512
                ${arg_ETC2_ARGS}
                ${arg_EXTRA_ARGS}
            COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${etc2_output}.tmp
                ${etc2_output}
            COMMENT "Generating ETC2 compressed image ${etc2_output}"
        )
    endif()

    set(${arg_OUTPUT_IMAGE_LIST} ${image_list} PARENT_SCOPE)

endfunction()


