#
# GLES3 Asset conversion utils
#

if (NOT TARGET modus_glsl_include)
    message(FATAL_ERROR "Could not find modus_glsl_include target")
endif()

if (NOT TARGET modus_mk_gpu_program)
    message(FATAL_ERROR "Could not find modus_mk_gpu_program")
endif()

if (NOT TARGET modus_gpuprog_reflect)
    message(FATAL_ERROR "Could not find modus_gpuprog_reflect")
endif()

set(MODUS_GLES3_SHADER_INCLUDE_DIR ${MODUS_ASSET_SOURCE_DIR}/engine/shaders/gles3 CACHE STRING INTERNAL FORCE)

find_program(glsl_validator_bin glslangValidator)

function(modus_gles3_process_shader_include input output)
    cmake_parse_arguments(arg ""
        ""
        "INCLUDE_PATHS" ${ARGN})

    set(validator_command)
    if (glsl_validator_bin)
        set(validator_command
            COMMAND ${glsl_validator_bin} "${output}"
        )
    endif()

    set(dep_file_args)
    set(dep_command_args)
    if (CMAKE_GENERATOR STREQUAL "Ninja" OR CMAKE_GENERATOR STREQUAL "Ninja Ninja Multi-Config")
        set(dep_file_args DEPFILE "${output}.dep")

        file(RELATIVE_PATH dep_file "${${CMAKE_PROJECT_NAME}_BINARY_DIR}" "${output}")
        set(dep_command_args
            --dep-output-file "${output}.dep"
            --dep-file "${dep_file}"
        )
    endif()

    add_custom_command(OUTPUT ${output}
        DEPENDS ${input}
        COMMAND modus_glsl_include
            ${dep_command_args}
            -i ${arg_INCLUDE_PATHS}
            -o "${output}"
            "${input}"
        ${validator_command}
        COMMENT "Processing includes for GLSL file: ${input}"
        WORKING_DIRECTORY
        ${dep_file_args}
   )
endfunction()

function(modus_make_gl_program target)

    cmake_parse_arguments(arg ""
        "OUTPUT;VERTEX;FRAGMENT;CPP_HEADER;CPP_NAMESPACE;CPP_NAMESPACE_NAME"
        "SHADERS;INCLUDE_PATHS" ${ARGN})

    get_filename_component(output_dir ${arg_OUTPUT} DIRECTORY)

    set(program_args "")
    set(reflect_args)
    set(dependencies "")
    set(shader_type_list "VERTEX" "FRAGMENT")
    foreach(shader_type IN LISTS shader_type_list)
        if (NOT arg_${shader_type})
            continue()
        endif()

        set(shader ${arg_${shader_type}})
        if (NOT IS_ABSOLUTE shader)
            set(shader "${CMAKE_CURRENT_SOURCE_DIR}/${shader}")
        endif()
        get_filename_component(shader_name ${shader} NAME)
        set(tmp_shader_dir "${CMAKE_CURRENT_BINARY_DIR}")
        set(shader_processed "${tmp_shader_dir}/${shader_name}")
        if ("${shader_type}" STREQUAL "VERTEX")
            list(APPEND program_args --vert ${shader_processed})
            list(APPEND reflect_args --vert ${shader_processed})
        elseif("${shader_type}" STREQUAL "FRAGMENT")
            list(APPEND program_args --frag ${shader_processed})
            list(APPEND reflect_args --frag ${shader_processed})
        else()
            message(FATAL_ERROR "Unknown shader type")
        endif()

        list(APPEND dependencies ${shader_processed})
        modus_gles3_process_shader_include(${shader} ${shader_processed}
            INCLUDE_PATHS "${MODUS_GLES3_SHADER_INCLUDE_DIR}" ${arg_INCLUDE_PATHS}
        )
    endforeach()

    get_filename_component(output_name ${arg_OUTPUT} NAME)
    set(shader_constant "${CMAKE_CURRENT_BINARY_DIR}/${output_name}.constants.json")

    set(cpp_args "")
    set(cpp_output "")
    set(cpp_copy_arg)
    if (arg_CPP_HEADER)
        get_filename_component(header_name "${arg_CPP_HEADER}" NAME)
        set(tmp_header "${tmp_shader_dir}/${header_name}")
        list(APPEND cpp_args "--cpp-header" "${tmp_header}")
        set(cpp_output "${arg_CPP_HEADER}")
        if (arg_CPP_NAMESPACE)
            list(APPEND cpp_args "--cpp-namespace" "${arg_CPP_NAMESPACE}")
        endif()

        if (arg_CPP_NAMESPACE_NAME)
            list(APPEND cpp_args "--cpp-namespace-name" "${arg_CPP_NAMESPACE_NAME}")
        endif()

        get_filename_component(cpp_file_dir "${arg_CPP_HEADER}" DIRECTORY)
        file(MAKE_DIRECTORY "${cpp_file_dir}")
        set(cpp_copy_arg COMMAND ${CMAKE_COMMAND} -E copy_if_different
            "${tmp_header}"
            "${arg_CPP_HEADER}"
        )
    endif()

    add_custom_command(OUTPUT "${shader_constant}" #${cpp_output}
        DEPENDS ${dependencies}
        COMMAND modus_gpuprog_reflect
            -o ${shader_constant}
            ${reflect_args}
            ${cpp_args}
            ${cpp_copy_arg}
    )
    add_custom_command(OUTPUT ${arg_OUTPUT}
         DEPENDS ${dependencies} ${shader_constant}
         COMMAND modus_mk_gpu_program
            -t gles3
            -o ${arg_OUTPUT}.tmp
            ${program_args}
            -c ${shader_constant}
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${arg_OUTPUT}.tmp
            ${arg_OUTPUT}
         COMMENT "Generating GPUProgram ${program_output}"
    )

    add_custom_target(${target}
        DEPENDS ${arg_OUTPUT}
    )

    add_dependencies(make_gles3_shaders ${target})
endfunction()

add_custom_target(make_gles3_shaders)
