   MPRG $            Wi��1ALg�������   @'  �.   .  -'  #version 300 es
precision highp float;
precision highp int;

#line       1        1 
//
// Shared Global Data
//
layout(std140) uniform FrameGlobals {
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 projection_view_matrix;
    mat4 inv_view_matrix;
    mat4 inv_proj_view_matrix;
    float tick_sec;
    float elapsed_sec;
    int screen_width;
    int screen_height;
    vec2 u_near_far;
};

vec3 get_camera_position() {
    return vec3(inv_view_matrix[3][0], inv_view_matrix[3][1], inv_view_matrix[3][2]);
}


#line      6        0 
#line       1        2 
// Light Calculation routines

#if !defined(SKIP_SHADOWS)
#line       1        1 
//
// Everything Shadow Related
//

uniform sampler2D u_shadow_map;

layout(std140) uniform ShadowBlock {
    mat4 u_shadow_vp;
    vec4 u_shadow_light_dir;
    int num_shadow_casters;
};

// Calculate whether the light space project coordinate has shadow coverage
// \return 1.0 on true, 0.0 on false.
float calculate_shadows(in vec3 proj_coord, in vec3 normal) {
    // Sample depth buffer
    float closest_depth = texture(u_shadow_map, proj_coord.xy).r;
    float current_depth = proj_coord.z;

    // early exit
    if (current_depth > 1.0) {
        return 0.0;
    }

    // check whether current position is in shadow
    // Apply bias based on the original light direction and the current normal
    // Helps with round objects
    float cos_theta = clamp(dot(normal, u_shadow_light_dir.xyz), 0.0, 1.0);
    float bias = 0.005*tan(acos(cos_theta));
    bias = clamp(bias, 0.0,0.005);

#if defined(USE_PCF)
    // Apply PCF Filtering with 16 samples
    float shadow = 0.0;
    vec2 texel_size = 1.0 / vec2(textureSize(u_shadow_map, 0));
    for(float x = -1.5; x <= 1.5; x+=1.0)
    {
        for(float y = -1.5; y <= 1.5; y+=1.0)
        {
            float pcf_depth = texture(u_shadow_map, proj_coord.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth - bias > pcf_depth ? 1.0 : 0.0;
        }
    }
    shadow /= 16.0;
    return shadow;
# else
    float pcf_depth = texture(u_shadow_map, proj_coord.xy).r;
    return current_depth - bias > pcf_depth ? 1.0 : 0.0;
#endif
}

float calculate_shadows_light_space(in vec4 position_light_space, in vec3 normal) {
    if (num_shadow_casters == 0) {
        return 0.0;
    }
    // perspective divide to convert to light screen space
    vec3 proj_coord = position_light_space.xyz / position_light_space.w;
    // normalize to [0 - 1]
    proj_coord = (proj_coord + 1.0) * 0.5;
    return calculate_shadows(proj_coord, normal);
}

float calculate_shadows_world_pos(in vec3 world_position, in vec3 normal) {
    if (num_shadow_casters == 0) {
        return 0.0;
    }
    // TODO: Bias Matrix ??
    // convert to light space
    vec4 position_light_space = u_shadow_vp * vec4(world_position, 1.0);
    // perspective divide to convert to light screen space
    vec3 proj_coord = position_light_space.xyz / position_light_space.w;
    // normalize to [0 - 1]
    proj_coord = (proj_coord + 1.0) * 0.5;
    return calculate_shadows(proj_coord, normal);
}

#line      5        0 
#endif

#define TILE_WIDTH 64
#define TILE_HEIGHT 64
#define USE_TILED_FORWARD 1

struct DirectionalLight {
    vec3 diffuse_color;
    float diffuse_intensity;
    vec3 direction;
    float paddingxx;
};

struct PointLight {
    vec3 diffuse_color;
    float diffuse_intensity;
    vec3 position;
    float radius;
};

layout(std140) uniform Lights {
    DirectionalLight u_directional_lights[4];
    PointLight u_point_lights[128];
    vec4 u_ambient;
    int u_num_directional_lights;
    int u_num_point_lights;
};

//#define USE_TOON_SHADING

// Generic light calculations, diffuse only
vec4 calculate_light(in vec3 color,
    in vec3 world_position,
    in vec3 normal,
    in vec3 light_dir,
    in float diffuse_intensity,
    in float specular_factor) {

#ifndef USE_TOON_SHADING
    float n_dot_l = dot(normal, light_dir);
    float diffuse_factor = n_dot_l;

    // calculate diffuse
    vec4 diffuse_color = vec4(0);
    if (diffuse_factor > 0.0) {
        diffuse_color = vec4(color
                * diffuse_intensity
                * diffuse_factor, 1.0);
    }

    // calculate specular
    vec3 view_dir = normalize(get_camera_position() - world_position);
    vec3 halfway_dir = normalize(light_dir + view_dir);

    //TODO: Specify shininess property
    const float shininess = 16.0;
    float specular = pow(max(dot(normal, halfway_dir), 0.0), shininess * shininess);
    //float specular = pow(max(dot(view_dir, reflect_dir), 0.0), shininess);
    vec4 specular_color = vec4(vec3(specular_factor * specular * color), 1.0);
    return diffuse_color + specular_color;
#else

    float n_dot_l = dot(normal, light_dir);
    float diffuse_factor = smoothstep(0.0, 0.05, n_dot_l);

    // calculate diffuse
    vec4 diffuse_color = vec4(0);
    if (diffuse_factor > 0.0) {
        diffuse_color = vec4(color
                * diffuse_intensity
                * diffuse_factor, 1.0);
    }

    // calculate specular
    vec3 view_dir = normalize(get_camera_position() - world_position);
    vec3 halfway_dir = normalize(light_dir + view_dir);

    //TODO: Specify shininess property
    const float shininess = 16.0;
    float specular = pow(max(dot(normal, halfway_dir), 0.0), shininess * shininess);
    float specular_smooth = smoothstep(0.005, 0.01, specular);
    vec4 specular_color = vec4(vec3(specular_factor * specular_smooth * color), 1.0);

#define USE_RIM
#ifdef USE_RIM
    // Calculate rim
    float rim_dot = 1.0 - max(dot(view_dir, normal), 0.0);
    const float rim_amount = 0.8;
    // We only want rim to appear on the lit side of the surface,
    // so multiply it by NdotL, raised to a power to smoothly blend it.
    const float rim_threshold = 0.1;
	float rim_intensity = rim_dot * pow(n_dot_l, rim_threshold);
    rim_intensity = smoothstep(rim_amount - 0.01, rim_amount + 0.01, rim_intensity);
    vec4 rim = vec4(vec3(rim_intensity), 1.0);
#else
    vec4 rim = vec4(0.0);
#endif
    return diffuse_color + specular_color + rim;

#endif // USE_TOON_SHADING
}

vec4 calculate_directional_light(in DirectionalLight light, in vec3 normal,
    in vec3 world_position, float specular_factor) {
    return calculate_light(light.diffuse_color,
        world_position,
        normal,
        light.direction,
        light.diffuse_intensity,
        specular_factor);
}

vec4 calculate_point_light(in PointLight light, in vec3 normal,
    in vec3 world_position, float specular_factor) {

    vec3 light_dir = light.position - world_position;
    float dist = length(light_dir);
    light_dir = normalize(light_dir);

    vec4 color = calculate_light(light.diffuse_color,
        world_position,
        normal,
        light_dir,
        light.diffuse_intensity,
        specular_factor);

    /*float attenuation = light.a_constant +
        (light.a_linear * dist) +
        (light.a_exp * (dist * dist));*/

    // Alternative
    //float attenuation = clamp(1.0 - (dist*dist)/(light.radius * light.radius), 0.0, 1.0);
    float attenuation = clamp(1.0 - dist/light.radius, 0.0, 1.0);
    attenuation *= attenuation;

    return color * attenuation;
}

#if defined(USE_TILED_FORWARD)
uniform highp usampler2D u_light_grid;
uniform highp usampler2D u_light_indices;
#endif

vec4 calculate_lights_forward(in vec3 position, in vec3 normal,
    float specular_factor, in vec3 emissive) {
    vec4 final_color = vec4(0);
    for(int i = 0; i < u_num_directional_lights; ++i) {
        final_color += calculate_directional_light(u_directional_lights[i], normal, position, specular_factor);
    }
#if defined(USE_TILED_FORWARD)
    if (u_num_point_lights != 0) {
        ivec2 tile_coord = ivec2(gl_FragCoord.xy / vec2(TILE_WIDTH, TILE_HEIGHT));
        uvec2 tile_info = texelFetch(u_light_grid, tile_coord,0).xy;

        uint index_offset = tile_info.x;
        uint light_count = tile_info.y;
        for(uint i = 0u; i < light_count; i++) {
            ivec2 index_coord = ivec2(index_offset + i, 0);
            uint light_index = texelFetch(u_light_indices, index_coord, 0).r;
            final_color += calculate_point_light(u_point_lights[light_index], normal,
                position, specular_factor);
        }
    }
#else
    for (int i = 0; i < u_num_point_lights;++i) {
        final_color += calculate_point_light(u_point_lights[i], normal, position, specular_factor);
    }
#endif
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
#if !defined(SKIP_SHADOWS)
    float shadow_factor = calculate_shadows_world_pos(position, normal);
#else
    float shadow_factor = 0.0;
#endif
    return ambient_color + ((1.0 - shadow_factor) * final_color) + vec4(emissive, 1.0);
}

vec4 calculate_directional_light_deferred(in vec3 world_position, in vec3 normal,
    int instance_id, float specular_factor, in vec3 emissive) {
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
#if !defined(SKIP_SHADOWS)
    float shadow_factor = calculate_shadows_world_pos(world_position, normal);
#else
    float shadow_factor = 0.0;
#endif
    return ambient_color + vec4(emissive, 1.0)
        + ((1.0 - shadow_factor)
            * calculate_directional_light(u_directional_lights[instance_id],
            normal, world_position, specular_factor));
}

vec4 calculate_point_light_deferred(in vec3 position, in vec3 normal,
        int instance_id, float specular_factor, in vec3 emissive) {
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
    return ambient_color + vec4(emissive,1.0)
        + calculate_point_light(u_point_lights[instance_id], normal, position, specular_factor);
}

#line      7        0 


layout(location = 0) out vec4 frag_color;

in vec3 var_position;
in vec3 var_normal;
in vec2 var_texture;

uniform vec4 u_diffuse;
uniform float u_specular;


void main() {
    vec3 normal = normalize(var_normal);
    vec4 color = u_diffuse * calculate_lights_forward(var_position, normal, u_specular, vec3(0.0));
    frag_color = color;
}
   �  #version 300 es
precision highp float;
precision highp int;

#line       1        1 

#define MODUS_VERTEX_SLOT 0
#define MODUS_NORMAL_SLOT 1
#define MODUS_TEXTURE_SLOT 2
#define MODUS_TANGENT_SLOT 3
#define MODUS_BINORMAL_SLOT 4
#define MODUS_COLOUR_SLOT 5
#define MODUS_COLOR_SLOT 5
#define MODUS_CUSTOM1_SLOT 6
#define MODUS_CUSTOM2_SLOT 7
#define MODUS_CUSTOM3_SLOT 8
#define MODUS_CUSTOM4_SLOT 9
#define MODUS_BONE_INDICES_SLOT 10
#define MODUS_BONE_WEIGHTS_SLOT 11



#line      2        0 
layout(location = MODUS_BONE_INDICES_SLOT) in uvec4 BONE_INDICES;
layout(location = MODUS_BONE_WEIGHTS_SLOT) in vec4 BONE_WEIGHTS;

#define MODUS_ANIMATION_MAX_SKELETON_BONES 16

layout(std140) uniform SkeletonAnimData {
    mat4[MODUS_ANIMATION_MAX_SKELETON_BONES] u_bone_transforms;
};

mat4 get_bone_transform() {
    mat4 bone_transform = u_bone_transforms[BONE_INDICES[0]] * BONE_WEIGHTS[0];
    bone_transform += u_bone_transforms[BONE_INDICES[1]] * BONE_WEIGHTS[1];
    bone_transform += u_bone_transforms[BONE_INDICES[2]] * BONE_WEIGHTS[2];
    bone_transform += u_bone_transforms[BONE_INDICES[3]] * BONE_WEIGHTS[3];
    return bone_transform;
}

#line      6        0 


layout(location = 0) in vec4 POSITION;
layout(location = 1) in vec3 NORMAL;
layout(location = 2) in vec2 TEXTURE;

uniform mat4 u_mvp;
uniform mat4 u_mv;
uniform mat3 u_normal_matrix;

out vec3 var_position;
out vec3 var_normal;
out vec2 var_texture;


void main() {

    mat4 bone_transform = get_bone_transform();

    vec4 position = bone_transform * POSITION;
    vec3 normal = (bone_transform * vec4(NORMAL, 0)).xyz;

    var_normal = normalize(u_normal_matrix * normal);
    var_position = vec3(u_mv * position);
    var_texture = TEXTURE;
    gl_Position = u_mvp * position;
}
      H   $      ����      u_light_indices ����      u_light_grid              u_shadow_map    	      �   �   �   �   p   D          ����         Lights  ����         FrameGlobals    ����         ShadowBlock   
      
            SkeletonAnimData    ����      
   u_specular  ����      	   u_diffuse   ����         u_normal_matrix ����         u_mv                    u_mvp   